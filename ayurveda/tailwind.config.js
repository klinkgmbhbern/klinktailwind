module.exports = {
  purge: {
      enabled: true,
      content: [
        '../web/typo3conf/ext/globetrottertourssite/**/*.html',
        '../web/typo3conf/ext/globetrottertourssite/**/*.js',
        '../web/typo3conf/ext/ayurvedasite/**/*.html',
        '../web/typo3conf/ext/ayurvedasite/**/*.js'
      ]
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
    sans: ['Nunito', 'sans-serif'],
    display: ['Nunito', 'sans-serif'],
    body: ['Nunito', 'sans-serif']
    },
    extend: {
      colors: {
          primary: '#EA755E',
          secondary: '#BD675F'
      }
    }
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('tailwindcss'),
    require('autoprefixer'),
  ],
}
