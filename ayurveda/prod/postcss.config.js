// postcss.config.js
module.exports = {
  plugins: [
    require('postcss-import'),
    require('tailwindcss')('./ayurveda/tailwind.config.js'),
    require('autoprefixer'),
    require('postcss-hash'),
    require('postcss-minify'),
  ]
}