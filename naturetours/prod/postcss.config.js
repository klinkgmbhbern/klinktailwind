// postcss.config.js
module.exports = {
  plugins: [
    require('postcss-import'),
    require('tailwindcss')('./naturetours/tailwind.config.js'),
    require('autoprefixer'),
    require('postcss-hash'),
    require('postcss-minify'),
  ]
}