const fs = require("fs");
const path = require("path");

const cssPath = 'Resources/Public/Css';

const getAllFiles = function(dirPath, arrayOfFiles) {
	files = fs.readdirSync(dirPath);
	arrayOfFiles = arrayOfFiles || [];
	files.forEach(function(file) {
		if (fs.statSync(dirPath + "/" + file).isDirectory()) {
			arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles);
		} else {
			if (path.extname(file) === '.typoscript') {
				arrayOfFiles.push(path.join(dirPath, "/", file));
			}
		}
	})
	return arrayOfFiles
}


let mode = '';
let target = '';
let buildfile = '';
let extpath = '';
let targetdir = '';
let targetfile = '';

if (typeof(process.argv[2]) !== 'undefined') {
	mode = process.argv[2];
}
if (typeof(process.argv[3]) !== 'undefined') {
	target = process.argv[3];
}

if (mode === 'dev') {
	buildfile = 'tailwind.css';
}
else {
	try {
		if (fs.existsSync('manifest.json')) {
	    	try {
				const data = fs.readFileSync('manifest.json', 'utf8');
				const manifest = JSON.parse(data);
				for (const key in manifest) {    			
					buildfile = manifest[key];
				}
			} catch (err) {
				console.log('ERROR: Can\'t read from manifest.json');
			}			
		}
	} catch (err) {
		console.log('ERROR: manifest.json not found');
	}
}

extpath = target.replace(/\/$/, "");
targetpath = extpath + '/' + cssPath;
targetfile = targetpath + '/' + buildfile;
if (buildfile && targetfile) {
	let cssFilename = buildfile;
	if (mode === 'prod') {
		buildfile = 'dist/' + buildfile;
	}
	try {
	 	fs.copyFile(buildfile, targetfile, 0, (err) => {
  			if (err) throw err;
  			console.log('Buildfile ' + buildfile + ' copied successfully to ' + targetfile);

  			let files = getAllFiles(extpath);
			const regexCss = /tailwind\.?\w*\.css/;

			for (const idx in files) {
				let fileContent = fs.readFileSync(files[idx], 'utf8');
				if (fileContent.search(regexCss) > -1) {
					fs.writeFile(files[idx], fileContent.replace(regexCss, cssFilename), function(err) {
						if(err) {
							return console.log(err);
						}
					});
		  			console.log('Typoscript ' + files[idx] + ' updated');
				}
			}
		});
	} catch (err) {
	  console.log('Could not copy ' + buildfile + ' to target ' + targetfile);
	}

	if (mode === 'prod') {
		const files = fs.readdirSync(targetpath);
		const cssFiles = [];
		const regexCssFile = /tailwind\.(\w+)\.css$/;

		for (const i in files) {
			let test = files[i].match(regexCssFile);
			if (test !== null) {
				cssFiles.push(test[0]);
			}
		}

		if (cssFiles.length > 5) {
			let oldest = null;
			let removeFilename = null;
			
			for (let i in cssFiles) {
				try {
					let stats = fs.statSync(targetpath + '/' + cssFiles[i]);
					if (oldest == null) {
						oldest = stats.birthtimeMs;
					}
					if (stats.birthtimeMs <= oldest) {
						oldest = stats.birthtimeMs;
						removeFilename = cssFiles[i];
					}

				} catch (err) {
	  				console.log('Could not stat ' + targetpath + '/' + cssFiles[i]);
				}

			}
			try {
				fs.unlinkSync(targetpath + '/' + removeFilename);
			} catch (err) {
  				console.log('Could not delete ' + targetpath + '/' + removeFilename);
			}
		}
	}
}

